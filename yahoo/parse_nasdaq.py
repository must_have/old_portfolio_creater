import requests
from bs4 import BeautifulSoup
import re
from DB import getProxy


class GetNasdaq:
    def __init__(self, symbol):
        self.symbol = symbol
        self.url = 'https://www.nasdaq.com/symbol/{}'.format(str(symbol))

    def parse_summary(self):
        for i in range(1, 3):
            try:
                proxy = {'ftp': getProxy.Proxy()}
                r = requests.get(self.url, proxies=proxy)
                text = r.text.encode('utf-8')
                soup = BeautifulSoup(text, "lxml")
                table = soup.findAll('div', {'class': 'table-table fontS14px'})
                first_self_table = table[0]
                second_self_table = table[1]
                strings = first_self_table.findAll('div', {'class': 'table-row'})
                # ----- 1 year target
                year_target = strings[0].findAll('div', {'class': 'table-cell'})
                year_target = year_target[1].text.split()

                # ----- 90 Day Avg. Daily Volume
                day_av90 = strings[3].findAll('div', {'class': 'table-cell'})
                day_av90 = day_av90[1].text.split()

                # ----- P/E Ratio
                pe_ratio = strings[7].findAll('div', {'class': 'table-cell'})
                pe_ratio = pe_ratio[1].text.split()

                return [year_target[0], day_av90[0], pe_ratio[0]]
            except:
                continue
        return []

