from DB import getDB
import numpy as np
from numpy import random, mean, var, std
import statsmodels.api as sm
from numpy import linalg
from c_list import List
import math


def covariation(a, b):
    a_mean = np.mean(a)
    b_mean = np.mean(b)
    len(a)
    len(b)
    sum = 0
    for i in range(0, len(a)):
        try:
            sum += ((a[i] - a_mean) * (b[i] - b_mean))
        except IndexError:
            return 0

    return sum / (len(a) - 1)


def correlation(x, y):
    pass


def dispersion(etalon):
    disp = var(etalon)
    return disp


def cramers_rule(A, B):
    main_det = np.linalg.det(A)
    A_ = A.copy()
    A_[0][0] = B[0]
    A_[1][0] = B[1]
    second_det = np.linalg.det(A_)
    a = second_det / main_det
    A_ = A.copy()
    A_[0][1] = B[0]
    A_[1][1] = B[1]
    third_det = np.linalg.det(A_)

    b = third_det / main_det
    return b


def regression(x, y):
    with_list = List()
    all_variables = len(x)
    summa_x = with_list.sum_of_list(x)
    summa_y = with_list.sum_of_list(y)
    x_square = [i * i for i in x]
    y_square = [i * i for i in y]
    suma_x_square = with_list.sum_of_list(x_square)
    summa_y_square = with_list.sum_of_list(y_square)
    i = 0
    x_on_y = []
    for data in x:
        xy = float(data) * float(y[i])
        x_on_y.append(xy)
        i += 1
    summa_x_on_y = with_list.sum_of_list(x_on_y)
    A = np.array([[float(all_variables), summa_x], [summa_x, suma_x_square]])
    B = np.array([summa_y, summa_x_on_y])
    b = cramers_rule(A, B)
    not_x = summa_x / all_variables
    not_y = summa_y / all_variables
    Sx = suma_x_square - (not_x ** 2)
    Sy = summa_y_square - (not_y ** 2)
    rxy = b * (math.sqrt(Sx) / math.sqrt(Sy))
    return [rxy, b]
