import re
import requests
from bs4 import BeautifulSoup
from termcolor import colored
from multiprocessing import Process
from DB import getDB


class Get_Yahoo:
    def __init__(self, current_volume=0, week_range52=0, price=0, m_cap=0, beta=0, dividents=0):
        self.DB = getDB.mySqlConnect()
        self.dividents = dividents
        self.beta = beta
        self.m_cap = m_cap
        self.price = price
        self.week_range52 = week_range52
        self.current_volume = current_volume
        self.list_stocks = {}
        self.__i = 0
        self.__dict_sector = {'names': ['financial', 'healthcare', 'services', 'utilities',
                                        'industrial_goods', 'basic_materials',
                                        'conglomerates', 'consumer_goods', 'technology']}

    def get_page(self):
        process = []
        for data in self.__dict_sector['names']:
            self.parse_count(data)
        print(self.__i)

    def parse_count(self, name_sector):
        link = 'https://finance.yahoo.com/screener/predefined/{}?offset=0&count=100'.format(name_sector)
        r = requests.get(link)
        text = r.text.encode('utf-8')
        soup = BeautifulSoup(text, "lxml")
        count = soup.find('span', {'data-reactid': 20})
        result = re.split(r'of', count.text)
        count = re.split(r' ', result[1])
        count = int(count[1])
        count = int(count / 100 + 1)
        i = 0
        flag = 0
        while i < count:
            self.get_stocks(name_sector, flag)
            flag += 100
            i += 1
            break

    def get_stocks(self, name_sector, flag):
        print(name_sector)
        link = 'https://finance.yahoo.com/screener/predefined/{}?offset={}&count=100'.format(name_sector, str(flag))
        r = requests.get(link)
        text = r.text.encode('utf-8')
        soup = BeautifulSoup(text, "lxml")
        table = soup.find('div', {'id': 'scr-res-table'})
        table_body = table.find('tbody')
        strings = table_body.findAll('tr')
        name = ''
        cursor = self.DB.cursor()
        for data in strings:
            try:
                data_company = data.findAll('td')
                symbol = data_company[0].text
                name = data_company[1].text
                name = re.sub(',', '', name)
                name = re.sub("'", "", name)
                price = data_company[2].text
                price = re.sub(',', '', price)  # заміна коми якшо є
                volume = data_company[5].text
                price_x_volume = float(price) * float(get_market_cup(volume))
                m_cap = data_company[7].text
                try:
                    print(symbol, name)
                    addon_data = parse_company(symbol)  # beta, week range, dividends, link,eps,week range high
                    try:
                        beta = addon_data[0][0]
                        week_range52 = addon_data[0][1]
                        dividents = addon_data[0][2]
                        link = addon_data[1]
                        week_range_high = addon_data[2]
                        eps = addon_data[3]
                    except IndexError:
                        print('Index out of the range')
                        continue
                    print(name)
                    cursor.execute(
                        "insert into company(`name`,`symbol`,`sector_name`,`price`,`volume`,`beta`,`dividents`,`m_cap`,`52_week_range`, `week_range_high`,`pricevolume`,`eps`,`link`)"
                        " values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" %
                        (str(name), str(symbol), str(name_sector), str(price), str(volume), str(beta), str(dividents),
                         str(m_cap),
                         str(week_range52), week_range_high, str(price_x_volume), str(eps), str(link)))
                    self.__i += 1
                except ValueError:
                    print('Error in parse_company func with this symbol:' + str(symbol))
                    # file = open("interface/files/" + symbol + ".txt", "w")
                    # file.write(str(name) + ',')
                    # file.write(str(symbol) + ',')
                    # file.write(str(price) + ',')
                    # file.write(str(volume) + ',')
                    # file.write(str(m_cap) + ',')
                    # file.write(str(beta) + ',')
                    # file.write(str(dividents) + ',')
                    # file.write(str(name_sector) + ',')
                    # file.write(str(link))
                    # file.close()
                    # self.list_stocks.update({name: {'Symbol': symbol, 'Current price': price,
                    #                                 'Current volume': volume, 'Capitalisation': m_cap,
                    #                                 'Current beta': beta, 'Dividents': dividents, }})
            except TypeError:
                print('Type Error in get_stocks function')

    # дана  функція є універсальною для будь яких вхідних трьох значення типу : (1231_123) для можливості розподілу та обробки
    def main_filter(self, parsed_price, parsed_volume, parsed_m_cap):
        parsed_m_cap = get_market_cup(parsed_m_cap)
        parsed_volume = get_market_cup(parsed_volume)
        # mod - modificator (<>=)
        price = self.price
        prices = price.split('_')

        price = prices[0]
        mod_price = prices[1]
        if not get_comparison(price, parsed_price, mod_price):
            return False
        volumes = self.current_volume.split('_')
        volume = volumes[0]
        mod_volume = volumes[1]
        if not get_comparison(volume, parsed_volume, mod_volume):
            return False
        m_caps = self.m_cap.split('_')
        m_cap = m_caps[0]
        mod_m_cap = m_caps[1]
        if not get_comparison(m_cap, parsed_m_cap, mod_m_cap):
            return False
        else:
            return True

    def internal_filter(self, parsed_beta, parsed_week_range, parsed_dividents):
        # mod - modificator (<>=)
        beta = self.beta
        betas = beta.split('_')
        beta = betas[0]
        mod_beta = betas[1]
        if not get_comparison(beta, parsed_beta, mod_beta):
            return False
        week_range = self.week_range52
        weeks = week_range.split('_')
        week_range = weeks[0]
        mod_week_range = weeks[1]
        if not get_comparison(week_range, parsed_week_range, mod_week_range):
            return False
        dividents = self.dividents
        dividentss = dividents.split('_')
        dividents = dividentss[0]
        mod_dividents = dividentss[1]
        if not get_comparison(dividents, parsed_dividents, mod_dividents):
            return False
        else:
            print('yes!')
            return True


def get_comparison(inputed_value, parsed_value, modificator):
    inputed_value = float(inputed_value)
    parsed_value = float(parsed_value)
    if modificator == '>=':
        if parsed_value >= inputed_value:
            return True
        else:
            return False

    elif modificator == '<':
        if parsed_value < inputed_value:
            return True
        else:
            return False

    elif modificator == '=':
        if parsed_value == inputed_value:
            return True
        else:
            return False


def get_market_cup(m_cap):
    # print(m_cap)
    cap = None
    try:
        split = m_cap.split('.')

        if re.search(r'B', split[1]) is not None:
            cap = split[0] + '000000000'
        elif re.search(r'M', split[1]) is not None:
            cap = split[0] + '000000'
        else:
            print('Error in get_market_cup function (get_yahoo):' + m_cap)
    except:
        if re.search(r'B', m_cap) is not None:
            cap = re.sub(r'B', '000000000', m_cap)
        elif re.search(r'M', m_cap) is not None:
            cap = re.sub(r'M', '000000', m_cap)
        else:
            m_cap = re.sub(',', '', m_cap)
            return m_cap

    return cap


def parse_company(symbol):
    link = 'https://finance.yahoo.com/quote/{}?p={}'.format(symbol, symbol)
    r = requests.get(link)
    text = r.text.encode('utf-8')
    soup = BeautifulSoup(text, "lxml")
    beta = soup.find('td', {'data-test': 'BETA_3Y-value'})
    week_range = soup.find('td', {'data-test': 'FIFTY_TWO_WK_RANGE-value'})
    divident = soup.find('td', {'data-test': 'DIVIDEND_AND_YIELD-value'})
    eps = soup.find('td', {'data-test': 'EPS_RATIO-value'})
    try:
        eps = eps.text
        divident = divident.text
        divident = divident.split(' ')
        divident = divident[1]
        divident = re.sub('[()%]', '', divident)

        # print(divident)
        beta = beta.text
        # вік рейндж ділиться пополам, видаляється кома і крапка і два нулі в кожному числі
        half = week_range.text.split(' - ')
        left_half = half[0].split('.')
        right_half = half[1].split('.')
        left_half = re.sub(',', '', left_half[0])
        right_half = re.sub(',', '', right_half[0])
        week_range = int(right_half) - int(left_half)
        range_and_beta = [[beta, week_range, float(divident)], link, right_half, eps]
        return range_and_beta
    except AttributeError as e:
        print('Error in parse_company func: ' + str(e))
        return []


def parse_statistics(symbol):
    link = 'https://finance.yahoo.com/quote/{}/key-statistics?p={}'.format(str(symbol), str(symbol))
    r = requests.get(link)
    text = r.text.encode('utf-8')
    soup = BeautifulSoup(text, "lxml")
    table = soup.findAll('table', {'class': 'table-qsp-stats Mt(10px)'})
    trading_info = table[7]
    vaulation_measures = table[0]
    management_effect = table[3]
    list_of_trading = trading_info.findAll('tr')

    # Mov. average 50 day
    ma_50 = list_of_trading[5].findAll('td')
    ma_50 = ma_50[1].text

    # MA 200 days
    ma_200 = list_of_trading[6].findAll('td')
    ma_200 = ma_200[1].text

    list_of_vaulation = vaulation_measures.findAll('tr')

    # price/sales
    ps = list_of_vaulation[5].findAll('td')
    ps = ps[1].text

    list_of_manag = management_effect.findAll('tr')

    # return on equity
    r_e = list_of_manag[1].findAll('td')
    r_e = r_e[1].text

    return [ma_50, ma_200, ps, r_e]
