from lxml import etree
import xml.etree.ElementTree
from DB import getDB


def parse_xml(xmltest, company):
    DB = getDB.mySqlConnect()
    tags = []
    date = []
    period = []
    value = []
    root = etree.fromstring(xmltest)
    for appt in root.getchildren():
        for elem in appt.getchildren():
            if elem.get('reportType') == 'A' and elem.get('period') == '3M':
                tags.append(elem.tag)
                date.append(elem.get('asofDate'))
                period.append(elem.get('period'))
                value.append(elem.text)
            if elem.get('reportType') is None:
                continue
    cursor = DB.cursor()
    i = 0
    for data in tags:
        cursor.execute("insert into datasummary(`tag`,`value`,`date`,`period`,`company`) "
                       "VALUES('%s','%s','%s','%s','%s') " % (tags[i], value[i], date[i], period[i], company))
        i += 1

