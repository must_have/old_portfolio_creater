import re
from interface import startUp
import get_yahoo
import os
import time
from DB import getDB
from IB import getter
from DB import creating_db
from parse_xml import parse_xml
from bs4 import BeautifulSoup
import requests
import ParseSnP
from mathematic import mathematic
import c_list
from datetime import datetime
import statsmodels.api as sm
from scipy.stats.mstats import zscore
import getFromBD
import write_tocsv
from termcolor import colored
import parse_nasdaq
from DB import getProxy
import csv


def datasummary():
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    cursor.execute('truncate datasummary')
    cursor.close()
    cursor = DB.cursor()
    cursor.execute('select symbol from company')
    count = cursor.rowcount
    half = 100 / count
    percent = 0
    for company in cursor:
        os.system('cls')
        print('Downloading:' + str(round(percent, 2)) + '%')
        percent += half
        try:
            df = getter.getDataIB(company[0])
            parse_xml(df, company[0])
        except:
            print('This company {} isnt exists here'.format(company[0]))


def HData():
    open = []
    high = []
    low = []
    close = []
    date = []
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    cursor.execute('truncate historical')
    cursor.execute('select symbol from company')
    count = cursor.rowcount
    half = 100 / count
    percent = 0
    for symbol in cursor:
        os.system('cls')
        print('Downloading:' + str(round(percent, 2)) + '%')
        df = getter.getHDataIB(symbol[0], '7 Y')
        percent += half
        i = 0
        cursors = DB.cursor()
        try:
            for data in df['close']:
                cursors.execute(
                    "insert into historical(`company`,`open`,`high`,`low`,`close`,`date`) VALUES ('%s','%s','%s','%s','%s','%s')"
                    % (symbol[0], df['open'][i], df['high'][i], df['low'][i], df['close'][i], df['date'][i]))
                i += 1
        except:
            print('Error data')
            continue


def getLists(name, this_date, old_date):
    DB = getDB.mySqlConnect()
    snp = []
    company = []
    company_data = []
    cursor = DB.cursor()
    cursor.execute(
        "select company,close from historical where company='{}' and date BETWEEN '{}' and '{}' order by date desc".format(
            name, old_date, this_date))
    i = 0
    for data in cursor:
        if i % 30 == 0:
            company.append(data[0])
            company_data.append(float(data[1]))
        i += 1
    cursor.close()
    cursor = DB.cursor()
    cursor.execute(
        "select close from snp where date BETWEEN '{}' and '{}' order by date desc".format(old_date, this_date))
    i = 0
    for data in cursor:
        if i % 30 == 0:
            snp.append(float(data[0]))
        i += 1
    cursor.close()
    return [snp, company_data]


def goRegression():
    DB = getDB.mySqlConnect()
    Beta = {}
    with_list = c_list.List
    cursor = DB.cursor()
    cursor.execute('select symbol from company')
    i = 0
    for data in cursor:
        print(data[0])
        lists = getLists(data[0])
        lists[1].reverse()
        lists[0].reverse()
        snp_percent = with_list.return_value(lists[0])
        company_percent = with_list.return_value(lists[1])
        # snp_x = sm.add_constant(snp_percent)
        # print(sm.OLS(company_percent, snp_x).fit().summary())
        cov = mathematic.covariation(company_percent, snp_percent)
        disp = mathematic.dispersion(snp_percent)
        regression = mathematic.regression(snp_percent, company_percent)  # returns rxy and beta
        beta = cov / disp
        Beta[data[0]] = beta
        i += 1


def goFundRegression():
    fundamental = [['Name', 'Sector', 'Beta', 'Correlation', 'Eps Beta', 'Eps Correlation', 'Total Revenue Beta',
                    'Total Revenue Correlation', 'Divivdends Beta', 'Dividends Correlation', 'Price x Volume',
                    'Week range high', '52 Week Range', 'Year Target', ' 90 Day Average Volume', 'P/E Ratio',
                    '50 day Moving average', '200 day Moving average', 'Price/Sales', 'Return on equity']]

    date = str(datetime.now())
    date = re.split(' ', str(date))
    date = re.split('-', date[0])
    year = int(date[0])
    old_date = str(year - 3) + '-' + date[1] + '-' + date[2]
    this_date = re.split(' ', str(datetime.now()))

    DB = getDB.mySqlConnect()
    lists_company = []
    cursor = DB.cursor()
    cursor.execute('select company from historical')
    buffer = ''
    for data in cursor:
        if data[0] == buffer:
            buffer = data[0]
        else:
            lists_company.append(data[0])
            buffer = data[0]
    cursor.close()

    cursor = DB.cursor()
    sector = []
    week_range52 = []
    week_range_high = []
    pricevolume = []
    for data in lists_company:
        cursor.execute(
            'select sector_name, 52_week_range, week_range_high, pricevolume from company where symbol="{}"'.format(
                data))
        for sectors in cursor:
            sector.append(sectors[0])
            week_range52.append(sectors[1])
            week_range_high.append(sectors[2])
            pricevolume.append(sectors[3])
    cursor.close()
    data_summary = {}
    i = 0
    count = len(lists_company)
    for data in lists_company:
        hdata = []
        snp_data = []
        data_summary.update(getFromBD.getDatasummary(DB, data))
        dividends = data_summary[data]['dividends'][0]
        dividends_date = data_summary[data]['dividends'][1]
        eps = data_summary[data]['EPS'][0]
        eps_date = data_summary[data]['EPS'][1]
        total_reenue = data_summary[data]['TotalRevenue'][0]
        Tr_date = data_summary[data]['TotalRevenue'][1]
        dividends.reverse()

        hdata = getLists(data, this_date[0], old_date)[1]
        snp_data = getLists(data, this_date[0], old_date)[0]  # temp
        date_for_summary = []
        for date in dividends_date:
            date_for_summary.append(getFromBD.getHData(date, data, DB))
        hdata.reverse()
        eps.reverse()
        snp_data.reverse()
        date_for_summary.reverse()
        try:
            total_reenue.reverse()
            dividends = c_list.List.return_value(dividends)
            hdata = c_list.List.return_value(hdata)
            date_for_summary = c_list.List.return_value(date_for_summary)
            eps = c_list.List.return_value(eps)
            total_reenue = c_list.List.return_value(total_reenue)
            snp_data = c_list.List.return_value(snp_data)
            print(data, sector[i])

        except:
            i += 1
            continue
        try:

            nasdaq = parse_nasdaq.GetNasdaq(data)
            statistic_data = get_yahoo.parse_statistics(data)
            ma_50 = statistic_data[0]
            ma_200 = statistic_data[1]
            ps = statistic_data[2]
            r_e = statistic_data[3]
            addon_data = nasdaq.parse_summary()
            year_target = addon_data[0]
            day_av90 = addon_data[1]
            pe_ratio = addon_data[2]

            fundamental.append([data, sector[i], mathematic.regression(snp_data, hdata)[1],
                                mathematic.regression(snp_data, hdata)[0],
                                mathematic.regression(eps, date_for_summary)[1],
                                mathematic.regression(eps, date_for_summary)[0],
                                mathematic.regression(total_reenue, date_for_summary)[1],
                                mathematic.regression(total_reenue, date_for_summary)[0],
                                mathematic.regression(dividends, date_for_summary)[1],
                                mathematic.regression(dividends, date_for_summary)[0], pricevolume[i],
                                week_range_high[i], week_range52[i], year_target, day_av90, pe_ratio, ma_50, ma_200, ps,
                                r_e])
            my_list = []
            fieldnames = fundamental[0]
            cell = fundamental[1:]
            for values in cell:
                inner_dict = dict(zip(fieldnames, values))
                my_list.append(inner_dict)
            path = "dict_output.csv"
            write = write_tocsv.toCsv(path, fieldnames, my_list)
            write.write()

            # print('Covarioation snp: ', mathematic.covariation(hdata, snp_data))
            # print('Covarioation EPS: ', mathematic.covariation(hdata, eps))
            # print('Covarioation TotalRevenue: ', mathematic.covariation(hdata, total_reenue))
            # print('Covarioation Dividents: ', mathematic.covariation(hdata, dividends))
            # print('Regression EPS: ', mathematic.regression(eps, hdata))
            # print('Regression TotalRevenue: ', mathematic.regression(total_reenue, hdata))
            # print('Regression dividends: ', mathematic.regression(dividends, hdata))
            # print('Regression snp: ', mathematic.regression(snp_data, hdata))

        except:
            i += 1
            continue
        i += 1


def snp():
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    cursor.execute('truncate snp')
    link = 'http://investfunds.ua/markets/indicators/sp-500/?get_xls&f_s[idx]=0&f_s[sdate]=24.04.2011&f_s[edate]='
    ParseSnP.findData(link, DB)


def getMaxRelations(relations_list=list()):
    symbol = list()
    relations = list()
    for data in relations_list:
        symbol.append(data[0])
        relations.append(float(data[1]))
    unsorted_relations = relations.copy()

    relations.sort()
    print('sorted:', relations)
    max_relation = relations[-1]
    try:
        sec_max_relation = relations[-2]
    except IndexError:
        print('Only one item')
    i = 0
    return_list = list()
    for data in unsorted_relations:
        if data == max_relation or data == sec_max_relation:
            return_list.append([symbol[i], data])
        i += 1

    return return_list


def strong_or_weak():
    DB = getDB.mySqlConnect()
    # relations_dict = {}
    # all_relations = []
    # cursor = DB.cursor()
    # cursor.execute('select symbol,price,eps from company where price !=0 and eps !=0')
    # company_data = cursor.fetchall()
    # cursor.close()
    # for data in company_data:
    #     eps = float(data[2])
    #     close = float(data[1])
    #     relation = eps / close
    #     all_relations.append(relation)
    #     relations_dict.update({data[0]: relation})
    # average = sum(all_relations)/len(all_relations)
    # i=0
    # flag=0
    # for data in all_relations:
    #
    #     if data>average:
    #         flag+=1
    #     i+=1
    cursor = DB.cursor()
    company_data = list()
    cursor.execute('select company,close from historical where date ="2018-06-29" and close!="0"')
    for data in cursor:
        company_data.append([data[0], data[1]])
    cursor.close()
    cursor = DB.cursor()
    all_relations = list()
    i = 0
    companies = company_data.copy()
    for data in company_data:
        print(data[0])
        cursor.execute(
            'select value from datasummary where tag="EPS" and company="{}" and value!="0" order by date desc'.format(
                data[0]))
        for epss in cursor:
            eps = epss[0]
            break
        try:
            if float(eps) > 55:
                del companies[i]
                continue
            all_relations.append(float(eps) / float(data[1]))
        except IndexError:
            del companies[i]
            continue
        i += 1
    cursor.close()

    cursor = DB.cursor()
    cursor.execute('select sector_name from company')
    sector_data = cursor.fetchall()

    middle = sum(all_relations) / len(all_relations)
    iterator = int()
    buffer = str()
    data_dict = dict()
    relations = list()
    print('buffer, data_dict and relations are created')
    for data in all_relations:
        sector = sector_data[iterator][0]
        if buffer != sector:
            relations = []
            buffer = sector
        if data > middle:
            relations.append([companies[iterator][0], data])
            data_dict.update({buffer: relations})
        iterator += 1

    new_portfolio_dict = dict()
    print(data_dict)
    for data in data_dict.keys():
        flag = 0
        buffer_list = []
        for relations in data_dict[data]:
            flag = relations[1]
            buffer_list.append([relations[0], flag])
        maxRelations = getMaxRelations(buffer_list)

        try:
            new_portfolio_dict.update({data: maxRelations})
        except IndexError:
            continue

    print(new_portfolio_dict)
    all_uniq_company = list()
    for data in new_portfolio_dict.keys():
        for keys in new_portfolio_dict[data]:
            all_uniq_company.append(keys[0])
    company_dict_old = dict()
    company_dict_new = dict()
    cursor = DB.cursor()
    for data in all_uniq_company:
        cursor.execute(
            'select close from historical where company="{}" and date BETWEEN "2018-06-20" AND "2018-06-30"'.format(
                data))
        for _ in cursor:
            close = _[0]
            break
        company_dict_old.update({data: close})
    cursor.close()
    cursor = DB.cursor()
    for data in all_uniq_company:
        cursor.execute(
            'select close from historical where company="{}" order by date desc'.format(data))
        for _ in cursor:
            close = _[0]
            break
        company_dict_new.update({data: close})
    cursor.close()
    print(company_dict_old)
    print(company_dict_new)
    fieldnames = []
    data_old = []
    data_new = []
    company_old = []
    company_new = []
    for data in company_dict_old.keys():
        company_old.append(data)
        data_old.append(company_dict_old[data])

    for data in company_dict_new.keys():
        company_new.append(data)
        data_new.append(company_dict_new[data])
    sectors_data = []
    for data in new_portfolio_dict.keys():
        sectors_data.append(data)

    prices = list()
    for data in company_dict_old.keys():
        prices.append(float(company_dict_old[data]))
    new_prices = list()
    for data in company_dict_new.keys():
        new_prices.append(float(company_dict_new[data]))

    sector_price_old = []
    i = 1
    buffer = 0
    for data in data_old:
        buffer += float(data)
        if i % 2 == 0:
            sector_price_old.append(buffer)
            buffer = 0
        i+=1
    sector_price_new = []
    i = 1
    buffer = 0
    for data in data_new:
        buffer += float(data)
        if i % 2 == 0:
            sector_price_new.append(buffer)
            buffer = 0
        i+=1

    purchased = sum(prices)
    balance = sum(new_prices)
    fieldnames.append(['Purchased on 29 July 2018 for {}$'.format(str(purchased))])
    fieldnames.append(company_old)
    fieldnames.append(data_old)
    fieldnames.append(['      '])
    fieldnames.append(['Balance now: {}$'.format(str(balance))])
    fieldnames.append(company_new)
    fieldnames.append(data_new)
    fieldnames.append(['Balance on 29 July 2018:'])
    fieldnames.append(sectors_data)
    fieldnames.append(sector_price_old)
    fieldnames.append(['      '])
    fieldnames.append(['Balance now:'])
    fieldnames.append(sectors_data)
    fieldnames.append(sector_price_new)
    with open("portfolio.csv", "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(fieldnames)


if __name__ == '__main__':
    while True:
        print('Is this the first launch? Y/N')
        check = input()

        if check == 'Y' or check == 'y' or check == 'yes':

            DB = getDB.mySqlConnect(None)
            create_db = creating_db.Create_schema(DB=DB, name='yahoo')
            create_db.drop()
            create_db.create()

            DB = getDB.mySqlConnect('yahoo')
            create_db = creating_db.Create_schema(DB=DB)

            create_db.yahoo_company()
            create_db.yahoo_datasummary()
            create_db.yahoo_historical()
            create_db.yahoo_snp()
            print('Get companies from yahoo:')
            getYahoo = get_yahoo.Get_Yahoo()
            getYahoo.get_page()
            print('Companies are downloaded')
            break
        elif check == 'N' or check == 'n' or check == 'no':
            break
        else:
            continue
    while True:
        print('1 - download historical data')
        print('2 - download datasummary (dividends,eps,revenue)')
        print('3 - download s&p500 data')
        print('4 - calculate regression')
        print('5 - check if stock strong or not')
        print('6 - exit')
        choice = input()
        if choice == '1':
            print('Wait a few minutes for downloading historical data:')
            # startUp.runner()
            try:
                HData()
            except:
                print(colored('Error whille downloading historical data. Please try again.', 'red'))
        elif choice == '2':
            try:
                print('Downloading datasummary')
                datasummary()
            except:
                print(colored('Error whille downloading datasummary. Please try again.', 'red'))
        elif choice == '3':
            print('Get s&p500 historical data:')
            snp()
        # goRegression()
        elif choice == '4':
            print('calculate regression, correlation')
            goFundRegression()
        elif choice == '5':
            strong_or_weak()
        elif choice == '6':
            break
        else:
            print('Error! Try again.')
