import csv


class toCsv:
    def __init__(self, path, fieldnames, my_list):
        self.my_list = my_list
        self.fieldnames = fieldnames
        self.path = path

    def write(self):
        """
        Функция для записи в файл csv
        path - путь до файла
        fieldnames - название столбцов
        data - список из списков
        """
        with open(self.path, "w", newline='') as out_file:
            '''
            out_file - выходные данные в виде объекта
            delimiter - разделитель :|;
            fieldnames - название полей (столбцов)
            '''
            writer = csv.DictWriter(out_file, delimiter=',', fieldnames=self.fieldnames)
            writer.writeheader()
            for row in self.my_list:
                writer.writerow(row)
        out_file.close()
