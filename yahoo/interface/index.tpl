<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>All companies</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="/static/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="/static/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="/static/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="/static/assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>
<body>

<style>
    h4 {
        /* Цвет фона под заголовком */
        color: green; /* Цвет текста */
        padding: 2px; /* Поля вокруг текста */
    }
</style>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="/">Finance</a>

        </div>

    </nav>
    <div id="page-wrapper" >

        <form class="layer1">
            <input value="Open" onclick=clickFunction() type="button">

        </form>
        <form class="layer2">
            <input value="Close" onclick=clickFunction1() type="button">

        </form>
        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">

                    <table class="table table-striped table-bordered table-hover dataTable no-footer" id="table1" aria-describedby="dataTables-example_info">

                    </table>

                </div>
            </div>
            <!-- /. ROW  -->
        </div>    <!-- /. PAGE WRAPPER  -->
    </div>
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="/static/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/static/assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/static/assets/js/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="/static/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/static/assets/js/morris/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/static/assets/js/custom.js"></script>


</body>
</html>

<script>
    function clickFunction() {
        $('#table1').load('http://localhost:4444/report/Open' , '#dataTables-example');
    }
    function clickFunction1() {
        $('#table1').load('http://localhost:4444/report/Close' , '#dataTables-example');
    }
</script>

<style>
    .layer1 {
        float: left;
        padding: 10px; /* Поля вокруг текста */
        margin-right: 20px; /* Отступ справа */
    }
    .layer2 {
        padding: 10px; /* Поля вокруг текста */
        margin-right: 20px; /* Отступ справа */
    }
</style>