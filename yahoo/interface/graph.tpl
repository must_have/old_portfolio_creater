<!doctype html>
<html>

<head>
    <title>Line Chart</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />

</head>
<body>

<div id="chartjs" style="width:100%;">
   <canvas id="myChart"></canvas>
</div>
<div>
    <table id ="status_table"  border="1" width="40%" cellpadding="5"   >
        <thead>
        <tr>
            <th>Company</th>
            <th>Status</th>
        </tr>
        </thead>

        <tbody>

        </tbody>
    </table>
</div>
<br>
<br>
<script>
    var d = document;


    function addRow(data)
    {
        // Считываем значения с формы
        var name = 'hello';
        var initials = 'hi';
        var posada = 'bubub';


        var tbody = d.getElementById('status_table').getElementsByTagName('TBODY')[0];
        console.log(data.status.length);
        for(var i=0;i<data.status.length; i++ ) {
            var row = d.createElement("TR");
            tbody.appendChild(row);

            var td1 = d.createElement("TD");
            var td2 = d.createElement("TD");

            row.appendChild(td1);
            row.appendChild(td2);
            td1.innerHTML = data.status[i][0];
            td2.innerHTML = data.status[i][1];
        }
}

    var ctx = document.getElementById('myChart').getContext('2d');
    jQuery.get( "http://localhost:4444/array/{{t}}", function(data) {
        addRow(data);
        var chart = new Chart(ctx, {
                type: 'line',
                data: {

                    labels: data.allData,
                    datasets: [

                                %i =0
                                %for iter in names_of_sector:
                                {   hidden: true,
                                    label:"{{iter[0]}}",
                                    backgroundColor:'rgba(255,255,255,0)',
                                    borderColor:'{{colors[i]}}',
                                    data:data.prices[{{iter[1]}}],
                                    %i+=1
                                },
                                % end
                            ]},
        options: {
                    legend: {
                        reverse:true
                    }},

        });
    });

</script>
</body>

</html>
