<!DOCTYPE HTML>
<html>

<head>
  <title>Finance</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="/bottles/style.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="http://localhost:4444/">To the <span class="logo_colour">start page</span></a></h1>
        </div>
      </div>
    </div>
    <div id="site_content">
	<div id="my_content">
      <form>
        <div>
          <select id="ddlViewBy"  class="layer2">
            <option value="1" selected="selected" > 1 month</option>
            <option value="3" >3 month</option>
            <option value="6">6 month</option>
            <option value="12">1 year</option>
          </select>
            <input type="button" class = "layer2" value="Get list of companies" onclick=clickFunction();>
            <input type="button" class = "layer2" value="Clear table " onclick=remove_table();>
        </div>
        <div>


        </div>
          <div id="myProgress">
              <div id="myBar"></div>
          </div>
      <div>
        <div id="self_my_table" >
        </div>
      </div>
</body>
</html>

<script>
    function remove_table() {
        $('#my_table').remove();

    }
    function clickFunction() {
          var checker;

        var e = document.getElementById("ddlViewBy");
        var strUser = e.options[e.selectedIndex].value;

        if(String(strUser) === '1')
        {
            checker='21';
        }
        if(String(strUser) === '3')
        {
            checker='63';
        }
        if(String(strUser) === '6')
        {
            checker='126';
        }        if(String(strUser) === '12')
        {
            checker='250';
        }

        $('#self_my_table').load('http://192.168.2.148:4444/table/'+checker , '#my_table');
        move();

    }
    function move() {
        var elem = document.getElementById("myBar");
        var width = 1;
        var id = setInterval(frame, 68);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
            }
        }
    }


</script>


<style>
    .layer1 {
      width:140px;
      height:10px;
    }
	.layer2
	{
      text-align:center;
	width:140px; 
	height:30px;
	}
    #myProgress {
        margin-top:10px ;
        width: 200%;
        background-color: grey;
    }
    #myBar {
        width: 0;
        height: 15px;
        background-color: green;
    }

</style>