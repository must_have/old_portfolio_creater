import os, shutil
from bottle import route, run, template, static_file
import mysql.connector
from bottle import response
import json
import re
from get_yahoo import Get_Yahoo
from DB import getDB

colors = ['rgb(30,50,255)', 'rgb(255,50,30)', 'rgb(50,255,30)', 'rgb(200,100,255)',
          'rgb(60,100,255)', 'rgb(255,100,60)', 'rgb(100,255,60)', 'rgb(255,100,255)',
          'rgb(80,120,255)', 'rgb(255,120,80)', 'rgb(120,140,80)', 'rgb(120,200,80)']


def runner(host='localhost', port=4444):
    run(host=host, port=port)


@route("/interface/<filepath:path>")
def server_static(filepath):
    return static_file(filepath, root='interface')


@route("/")
def start_page():
    return template('interface/start_page.tpl')


@route('/getData/<price>/<volume>/<m_cap>/<beta>/<dividents>/<week_range>')
def getData(price, volume, m_cap, beta, dividents, week_range):
    DB = getDB.mySqlConnect()
    list_stocks = {}
    m_caps = m_cap.split('_')
    m_cap = float(m_caps[0]) * 1000000000
    m_cap = str(m_cap) + '_' + m_caps[1]

    volumes = volume.split('_')
    volume = float(volumes[0]) * 1000000
    volume = str(volume) + '_' + volumes[1]

    get_yahoo = Get_Yahoo(volume, week_range, price, m_cap, beta, dividents)
    cursor = DB.cursor()
    cursor.execute(
        "select * from company")  # 1-name, 2-symbol, 3-sector, 4-price, 5-volume, 6-beta, 7-div, 8-cap, 9-range, 10-link

    for data in cursor:
        if not get_yahoo.main_filter(data[4], data[5], data[8]):
            continue
        elif get_yahoo.internal_filter(data[6], data[9], data[7]):
            list_stocks.update({data[1]: {'Sector': data[3], 'Symbol': data[2], 'Current price': data[4],
                                          'Current volume': data[5], 'Capitalisation': data[8],
                                          'Current beta': data[6], 'Dividents': data[7], 'Link': data[10],
                                          'Growth': data[9]}})

    return template('interface/main_table.tpl', list_stocks=list_stocks)
