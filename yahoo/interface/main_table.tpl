
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>All companies</title>

</head>
<body>
<div>

                    <table id="my_table" width="100%">
                        <thead >
                        <tr role="row">
                            <th rowspan="1" colspan="1"  style="width: 164px;">#</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;">Sector</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;">Symbol</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;">Price</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;">Volume</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;"> Capitalisation</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;"> Beta</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;"> Dividents</th>
                            <th rowspan="1" colspan="1"  style="width: 164px;"> Growth</th>
                        </tr>
                        </thead>
                        <tbody>
                        % i = 1
                        % for iter in list_stocks:


                        <tr class="gradeA odd">
                            <td >{{i}}</td>
                            <td > {{list_stocks[iter]['Sector']}}</a></td>
                            <td ><a href="{{list_stocks[iter]['Link']}}" target="_blank">{{list_stocks[iter]['Symbol']}}</td>
                            <td >{{list_stocks[iter]['Current price']}}</td>
                            <td >{{list_stocks[iter]['Current volume']}}</td>
                            <td >{{list_stocks[iter]['Capitalisation']}}</td>
                            <td >{{list_stocks[iter]['Current beta']}}</td>
                            <td >{{list_stocks[iter]['Dividents']}}</td>
                            <td >{{list_stocks[iter]['Growth']}}</td>
                            % i += 1
                        </tr>
                        % end

                        </tbody>
                    </table>

</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="/static/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/static/assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/static/assets/js/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="/static/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/static/assets/js/morris/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/static/assets/js/custom.js"></script>
<script src ="/static/assets/js/jparcer.js"></script>

</body>
</html>

<script>
    function move() {
        var elem = document.getElementById("myBar");
        var width = 1;
        var id = setInterval(frame, 68);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
            }
        }
    }
</script>