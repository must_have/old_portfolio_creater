<!doctype html>
<html>

<head>
    <title>Line Chart</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />

</head>
<body>
<div>
</div>
<div id="chartjs" style="width:100%;">
   <canvas id="myChart"></canvas>
</div>
<br>
<br>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    jQuery.get( "http://localhost:4444/company/graph/{{name}}/{{t}}", function(data) {
        var chart = new Chart(ctx, {
                type: 'line',
                data: {

                    labels: data.allData,
                    datasets: [

                                %i =0
                                %for iter in list_of_legend:
                                {   hidden: false,
                                    label:"{{iter[0]}}",
                                    backgroundColor:'rgba(255,255,255,0)',
                                    borderColor:'{{colors[i]}}',
                                    data:data.prices[{{iter[1]}}],
                                    %i+=1
                                },
                                % end
                            ]},
        options: {
                    },

        });
    });

</script>
</body>

</html>
