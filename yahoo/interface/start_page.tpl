<!DOCTYPE HTML>
<html>

<head>
  <title>Finance</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="/interface/style.css" />
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

</head>

<body>
<div id="main">
  <div id="header">
    <div id="logo">
      <div id="logo_text">
        <!-- class="logo_colour", allows you to change the colour of the text -->
        <h1><a href="http://192.168.2.148:4444/">FinaNce<span class="logo_colour">filter</span></a></h1>
      </div>
    </div>
  </div>
  <div id="site_content">

    <div id="my_content">
      <input value="Search stocks"  type="button" class = "layer1" onclick=clickFunction(); >
      <div class="layer3">
        <h>Price $:</h>
        <select id="price"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="price_in" class="layer2" type="text" size="10">

        <h >  Volume (M):</h>
        <select id="volume"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="volume_in" class="layer2" type="text" size="10">
        <h>  Market cap (B):</h  >
        <select id="m_cap"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="m_cap_in" class="layer2" type="text" size="10">
        <h>  Beta:</h>
        <select  id="beta"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="beta_in" class="layer2" type="text" size="10">
        <h>  Dividents (Yield %):</h>
        <select id="dividents"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="dividents_in" class="layer2" type="text" size="10">
        <h>  Growth (52 weeks) $:</h>
        <select id="growth"  class="layer4">
          <option value=">=" selected="selected" > >= </option>
          <option value="<" ><</option>
          <option value="=" >=</option>
        </select>
        <input id="growth_in" class="layer2" type="text" size="10">

      </div>

    </div>
  </div>
  <div id="site_content1" >
    <div id="content" >
      <div id='tables' style="width:200%;" class="layer2">
        <table id="table_main"></table>
      </div>
    </div>
  </div>

</body>
</html>

<script>

  function clickFunction() {
        var pri_ce = document.getElementById("price");
        var pri_cein = document.getElementById("price_in");
        var price = pri_ce.options[pri_ce.selectedIndex].value;
        var value_price = pri_cein.value;

        var vo_lume = document.getElementById("volume");
        var vo_lumein = document.getElementById("volume_in");
        var volume = vo_lume.options[vo_lume.selectedIndex].value;
        var value_volume = vo_lumein.value;

        var mcap = document.getElementById("m_cap");
        var mcapin = document.getElementById("m_cap_in");
        var m_cap = mcap.options[mcap.selectedIndex].value;
        var value_m_cap = mcapin.value;

        var be_ta = document.getElementById("beta");
        var be_tain = document.getElementById("beta_in");
        var beta = be_ta.options[be_ta.selectedIndex].value;
        var value_beta= be_tain.value;

        var divi_dents = document.getElementById("dividents");
        var divi_dentsin = document.getElementById("dividents_in");
        var dividents = divi_dents.options[divi_dents.selectedIndex].value;
        var value_dividents = divi_dentsin.value;

        var grow_th = document.getElementById("growth");
        var grow_thin = document.getElementById("growth_in");
        var growth = grow_th.options[grow_th.selectedIndex].value;
        var value_growth = grow_thin.value;

        if (Number(value_price)>=0 && Number(value_beta)>=0 && Number(value_m_cap)>=0 && Number(value_dividents)>=0 && Number(value_growth)>=0 && Number(value_volume)>=0){
            var make_price = value_price +'_'+price;
            var make_volume = value_volume +'_'+volume;
            var make_m_cap = value_m_cap +'_'+m_cap;
            var make_beta = value_beta +'_'+beta;
            var make_dividents = value_dividents +'_'+dividents;
            var make_growth = value_growth +'_'+growth;

            $('#table_main').load('http://192.168.2.148:4444/getData/'+make_price+'/'+make_volume+'/'+make_m_cap+'/'+make_beta+'/'+make_dividents+'/'+make_growth, '#my_table');
        }
        else{
            alert('Fields are empty || Type error')
        }




    }


</script>


<style>
  .layer1 {
  ;
    width:140px;
    height:30px;
  }
  .layer2

  { text-align: center;
    margin-top: 2px;
    margin-bottom: 2px;
    left: 18%;
    width: 60px;
  }
  .layer3
  {
    width: 1920px;
  }
  .layer4
  {
    text-align: center;
    margin-top: 2px;
    margin-bottom: 2px;
    left: 18%;


  }
</style>