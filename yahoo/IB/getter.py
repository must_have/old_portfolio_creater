from ib_insync import *
from ibapi import tag_value


def getDataIB(symbol):
    ib = IB()
    ib.connect('127.0.0.1', 7496, clientId=2)
    contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
    # bars = ib.reqHistoricalData(contract, endDateTime='', durationStr='1 Y',
    #                           barSizeSetting='1 day', whatToShow='MIDPOINT', useRTH=True)
    bars = ib.reqFundamentalData(contract, 'ReportsFinSummary')
    # df = util.df(bars)
    return bars


def getHDataIB(symbol, duration):
    ib = IB()
    ib.connect('127.0.0.1', 7496, clientId=3)
    contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
    bars = ib.reqHistoricalData(contract, endDateTime='', durationStr=duration,
                                barSizeSetting='1 day', whatToShow='MIDPOINT', useRTH=True)
    df = util.df(bars)
    return df
