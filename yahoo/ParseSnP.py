import requests
from bs4 import BeautifulSoup
import urllib.request
import csv
import re
from xlrd import xldate_as_tuple
from datetime import datetime
import xlrd
from urllib.request import urlretrieve


def findData(link, DB):
    destination = 'snp.xls'
    sp500(destination, DB, link)


def sp500(destination, DB, link):
    urlretrieve(link, destination)
    rb = xlrd.open_workbook(destination, formatting_info=True)
    sheet = rb.sheet_by_index(0)
    i = 0
    price = []
    dates = []
    for rownum in range(sheet.nrows):
        if i <= 2:
            i += 1
            continue
        row = sheet.row_values(rownum)
        if row[0] == '':
            break
        date = row[0].replace(".", '-')
        try:
            d = datetime.strptime(date, "%d-%m-%Y")
            dates.append(str(d.date()))
        except ValueError:
            d = datetime.now().strftime("%Y-%m-%d")
            dates.append(str(d))
        price.append(row[1])

    i = 0
    cursor = DB.cursor()
    for _ in dates:
        cursor.execute("insert into `snp` (`close`, `date`) VALUES('%s', '%s')" %
                       (price[i], dates[i]))
        i += 1
    cursor.close()
