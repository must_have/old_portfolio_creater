from termcolor import colored

from DB import getDB
from fileWork import fileRule as fileR

DB = getDB.mySqlConnect()


def checkSectorTable():

    cursor = DB.cursor()
    cursor.execute('truncate sector')
    cursor.execute('select `Sector_Name` from sector')
    flag = 0
    for data in cursor:
        flag += 1
    if flag == 12:  # 11 Sectors and S&P
        print(colored('Table full: 12 sectors', 'blue'))
        return True
    else:
        print(colored('Table without data. Importing sectors into table.', 'blue'))
        fileR.readWriteSector()
        fileR.addSectorToDB(DB)
        return False
