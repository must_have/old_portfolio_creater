import mysql.connector
import MySQLdb


def mySqlConnect(name='yahoo'):
    try:
        if name is None:
            DB = MySQLdb.connect(user='root', password='',
                                 host='localhost')
        else:
            DB = MySQLdb.connect(user='root', password='',
                                 host='localhost', database=name)
        return DB
    except MySQLdb.Error as e:
        print('Error: ' + str(e))
        return False
